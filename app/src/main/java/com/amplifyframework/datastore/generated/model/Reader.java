package com.amplifyframework.datastore.generated.model;

import com.amplifyframework.core.model.temporal.Temporal;

import java.util.List;
import java.util.UUID;
import java.util.Objects;

import androidx.core.util.ObjectsCompat;

import com.amplifyframework.core.model.Model;
import com.amplifyframework.core.model.annotations.Index;
import com.amplifyframework.core.model.annotations.ModelConfig;
import com.amplifyframework.core.model.annotations.ModelField;
import com.amplifyframework.core.model.query.predicate.QueryField;

import static com.amplifyframework.core.model.query.predicate.QueryField.field;

/** This is an auto generated class representing the Reader type in your schema. */
@SuppressWarnings("all")
@ModelConfig(pluralName = "Readers")
public final class Reader implements Model {
  public static final QueryField ID = field("Reader", "id");
  public static final QueryField NAME = field("Reader", "name");
  public static final QueryField AGE = field("Reader", "age");
  public static final QueryField GENDER = field("Reader", "gender");
  private final @ModelField(targetType="ID", isRequired = true) String id;
  private final @ModelField(targetType="String", isRequired = true) String name;
  private final @ModelField(targetType="Int", isRequired = true) Integer age;
  private final @ModelField(targetType="String", isRequired = true) String gender;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime createdAt;
  private @ModelField(targetType="AWSDateTime", isReadOnly = true) Temporal.DateTime updatedAt;
  public String getId() {
      return id;
  }
  
  public String getName() {
      return name;
  }
  
  public Integer getAge() {
      return age;
  }
  
  public String getGender() {
      return gender;
  }
  
  public Temporal.DateTime getCreatedAt() {
      return createdAt;
  }
  
  public Temporal.DateTime getUpdatedAt() {
      return updatedAt;
  }
  
  private Reader(String id, String name, Integer age, String gender) {
    this.id = id;
    this.name = name;
    this.age = age;
    this.gender = gender;
  }
  
  @Override
   public boolean equals(Object obj) {
      if (this == obj) {
        return true;
      } else if(obj == null || getClass() != obj.getClass()) {
        return false;
      } else {
      Reader reader = (Reader) obj;
      return ObjectsCompat.equals(getId(), reader.getId()) &&
              ObjectsCompat.equals(getName(), reader.getName()) &&
              ObjectsCompat.equals(getAge(), reader.getAge()) &&
              ObjectsCompat.equals(getGender(), reader.getGender()) &&
              ObjectsCompat.equals(getCreatedAt(), reader.getCreatedAt()) &&
              ObjectsCompat.equals(getUpdatedAt(), reader.getUpdatedAt());
      }
  }
  
  @Override
   public int hashCode() {
    return new StringBuilder()
      .append(getId())
      .append(getName())
      .append(getAge())
      .append(getGender())
      .append(getCreatedAt())
      .append(getUpdatedAt())
      .toString()
      .hashCode();
  }
  
  @Override
   public String toString() {
    return new StringBuilder()
      .append("Reader {")
      .append("id=" + String.valueOf(getId()) + ", ")
      .append("name=" + String.valueOf(getName()) + ", ")
      .append("age=" + String.valueOf(getAge()) + ", ")
      .append("gender=" + String.valueOf(getGender()) + ", ")
      .append("createdAt=" + String.valueOf(getCreatedAt()) + ", ")
      .append("updatedAt=" + String.valueOf(getUpdatedAt()))
      .append("}")
      .toString();
  }
  
  public static NameStep builder() {
      return new Builder();
  }
  
  /** 
   * WARNING: This method should not be used to build an instance of this object for a CREATE mutation.
   * This is a convenience method to return an instance of the object with only its ID populated
   * to be used in the context of a parameter in a delete mutation or referencing a foreign key
   * in a relationship.
   * @param id the id of the existing item this instance will represent
   * @return an instance of this model with only ID populated
   */
  public static Reader justId(String id) {
    return new Reader(
      id,
      null,
      null,
      null
    );
  }
  
  public CopyOfBuilder copyOfBuilder() {
    return new CopyOfBuilder(id,
      name,
      age,
      gender);
  }
  public interface NameStep {
    AgeStep name(String name);
  }
  

  public interface AgeStep {
    GenderStep age(Integer age);
  }
  

  public interface GenderStep {
    BuildStep gender(String gender);
  }
  

  public interface BuildStep {
    Reader build();
    BuildStep id(String id);
  }
  

  public static class Builder implements NameStep, AgeStep, GenderStep, BuildStep {
    private String id;
    private String name;
    private Integer age;
    private String gender;
    @Override
     public Reader build() {
        String id = this.id != null ? this.id : UUID.randomUUID().toString();
        
        return new Reader(
          id,
          name,
          age,
          gender);
    }
    
    @Override
     public AgeStep name(String name) {
        Objects.requireNonNull(name);
        this.name = name;
        return this;
    }
    
    @Override
     public GenderStep age(Integer age) {
        Objects.requireNonNull(age);
        this.age = age;
        return this;
    }
    
    @Override
     public BuildStep gender(String gender) {
        Objects.requireNonNull(gender);
        this.gender = gender;
        return this;
    }
    
    /** 
     * @param id id
     * @return Current Builder instance, for fluent method chaining
     */
    public BuildStep id(String id) {
        this.id = id;
        return this;
    }
  }
  

  public final class CopyOfBuilder extends Builder {
    private CopyOfBuilder(String id, String name, Integer age, String gender) {
      super.id(id);
      super.name(name)
        .age(age)
        .gender(gender);
    }
    
    @Override
     public CopyOfBuilder name(String name) {
      return (CopyOfBuilder) super.name(name);
    }
    
    @Override
     public CopyOfBuilder age(Integer age) {
      return (CopyOfBuilder) super.age(age);
    }
    
    @Override
     public CopyOfBuilder gender(String gender) {
      return (CopyOfBuilder) super.gender(gender);
    }
  }
  
}
