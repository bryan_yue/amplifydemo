package com.example.amplifydemo.app.util

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.amplifyframework.auth.AuthUserAttributeKey
import com.amplifyframework.core.Amplify
import com.blankj.utilcode.util.StringUtils
import com.blankj.utilcode.util.ToastUtils
import com.example.amplifydemo.R
import com.example.amplifydemo.app.ext.nav
import com.example.amplifydemo.app.ext.navigateAction
import com.example.amplifydemo.data.entity.AssemblyEntity
import com.example.amplifydemo.ui.fragment.assembly.AssemblyFragment
import com.example.amplifydemo.ui.fragment.assembly.AssemblyViewModel
import java.util.*

object ModelUtil {

    fun getCategories_UserAttributes(): ArrayList<String> {
        val list: ArrayList<String> = arrayListOf()
        list.addAll(StringUtils.getStringArray(R.array.UserAttributes))
        return list
    }

    fun getAssembly_UpdateUserAttributesWithOutConfirm(dataId: String): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.TEXT_TITLE, dataId, ""))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "updateUserAttributes $dataId", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        return list
    }

    fun getAssembly_FetchUserAttributes(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "正在获取用户信息。。。", ""))

        return list
    }

    fun getAssembly_fetchAuthSession(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "正在获取用户凭证。。。", ""))

        return list
    }


    fun getAssembly_storage(dataId: String): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "Storage $dataId", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        return list
    }

    fun getAssembly_createandupdate_book(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入书名"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入作者"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入描述"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入状态"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "create-and-update-Book", ""))
        return list
    }

    fun getAssembly_createandupdate_author(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入作者姓名"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入作者年龄"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入作者性别"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "create-and-update-Author", ""))
        return list
    }


    fun getAssembly_createandupdate_Readers(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入读者姓名"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入读者年龄"))
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "请输入读者性别"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "create-and-update-Readers", ""))
        return list
    }

    fun getqueryBook(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "书搜索"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "query-Book", ""))
        return list
    }
    fun queryAuthor(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "作者搜索"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "query-Author", ""))
        return list
    }
    fun queryReaders(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "读者搜索"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "query-Reader", ""))
        return list
    }
    fun queryRelationsBook(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "作者名搜索书"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "querying-relations-Book", ""))
        return list
    }
    fun queryRelationsAuthor(): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        list.add(AssemblyEntity(AssemblyEntity.EditText, "", "书名搜索作者"))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_INFO, "", ""))
        list.add(AssemblyEntity(AssemblyEntity.TEXT_BUTTON, "querying-relations-Author", ""))
        return list
    }
    fun getData(dataId: String): ArrayList<AssemblyEntity> {
        val list: ArrayList<AssemblyEntity> = arrayListOf()
        when (dataId) {
            "获取当前用户属性" -> {
                list.addAll(getAssembly_FetchUserAttributes())
            }
            "uploadInputStream", "uploadFile", "downloadFile", "storage-list" -> {
                list.addAll(getAssembly_storage(dataId))
            }
            "access_credentials" -> {
                list.addAll(getAssembly_fetchAuthSession())
            }
            "create-and-updateBook" -> {
                list.addAll(getAssembly_createandupdate_book())
            }
            "create-and-updateAuthor" -> {
                list.addAll(getAssembly_createandupdate_author())
            }
            "create-and-updateReaders"-> {
                list.addAll(getAssembly_createandupdate_Readers())
            }
            "queryBook"-> {
                list.addAll(getqueryBook())
            }
            "queryAuthor"-> {
                list.addAll(queryAuthor())
            }
            "queryReaders"->{
                list.addAll(queryReaders())
            }
            "querying-relationsBook"-> {
                list.addAll(queryRelationsBook())
            }
            "querying-relationsAuthor"-> {
                list.addAll(queryRelationsAuthor())
            }
            else -> {
                list.addAll(getAssembly_UpdateUserAttributesWithOutConfirm(dataId))

            }
        }


        return list
    }

    fun ItemClick(fragment: Fragment, viewModel: ViewModel, action: String?) {
        Log.e("ItemClick", "action $action")
        when (action) {
            "Authentication" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putString("arrayId", R.array.auth.toString())
                        putString("arrayName", "Authentication")
                    })
            }
            "Storage" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putString("arrayId", R.array.Storage.toString())
                        putString("arrayName", "Storage")
                    })
            }
            "DataStore"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putString("arrayId", R.array.model.toString())
                        putString("arrayName", "DataStore")
                    })

            }
            "API (GraphQL)"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putString("arrayId", R.array.model.toString())
                        putString("arrayName", "DataStore")
                    })

            }
            "register-a-user" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_registerUserFragment
                )

            }

            "sign-in-a-user" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_signUserfFragment
                )
            }

            "devices" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_devicesFragment
                )
            }
            "password-management" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_passwordManagementFragment
                )
            }

            "sign-out" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_signOutFragment
                )
            }
            "user-attributes" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putString("arrayId", R.array.UserAttributes.toString())
                        putString("arrayName", "Authentication")
                    })
            }

            "access_credentials" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                    })
            }

            "获取当前用户属性" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", "获取当前用户属性")
                    })
            }
            "更新用户属性" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putStringArray("arrayData", getAuthUserAttributeKey())
                        putString("arrayName", "Authentication")
                    })
            }
            "ADDRESS", "BIRTHDATE" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                    })
            }

            "uploadInputStream", "uploadFile" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "Storage")
                    })
            }

            "storage-list", "storage-remove", "track-download-progress", "downloadFile" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_storageragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "Storage")
                    })
            }

            "Storage uploadFile", "Storage uploadInputStream" -> {
                val model: AssemblyViewModel = viewModel as AssemblyViewModel
                val assemblyFragment: AssemblyFragment = fragment as AssemblyFragment
                model.openFileManage(assemblyFragment)
            }
            "updateUserAttributes ADDRESS", "updateUserAttributes BIRTHDATE" -> {
                val model: AssemblyViewModel = viewModel as AssemblyViewModel
                model.updateUserAttributes(action)
            }
            "Author", "Book","Readers" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_categoriesFragment,
                    Bundle().apply {
                        putString("arrayId", R.array.datastore.toString())
                        putString("arrayName", "DataStore")
                        putString("model", action)
                    })
            }
            "create-and-updateBook" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "DataStore")
                        putString("model", "Book")
                    })
            }
            "create-and-updateAuthor" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "DataStore")
                        putString("model", "Author")
                    })
            }
            "create-and-updateReaders" -> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "DataStore")
                        putString("model", "Author")
                    })
            }
            "Delete"-> {
                Amplify.DataStore.stop({
                    Log.i("MyAmplifyApp", "DataStore stoped")
                },{
                    Log.e("MyAmplifyApp", "Error stoping DataStore", it)
                })
                Amplify.DataStore.clear(
                    { Log.i("MyAmplifyApp", "DataStore cleared") },
                    { Log.e("MyAmplifyApp", "Error clearing DataStore", it) }
                )
            }
            "deleteBook"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_datastoreFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "DataStore")
                        putString("model", "Book")
                    })
            }
            "deleteAuthor"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_datastoreFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "DataStore")
                        putString("model", "Author")
                    })
            }
            "deleteReaders"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_datastoreFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "GraphQL")
                        putString("model", "Reader")
                    })
            }
            "queryBook"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId", action)
                        putString("arrayName", "DataStore")
                        putString("model", "Book")
                    })
            }
            "queryAuthor"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId",  action)
                        putString("arrayName", "DataStore")
                        putString("model", "Author")
                    })
            }
            "queryReaders"-> {
                fragment.nav().navigateAction(
                    R.id.action_categoriesFragment_to_assemblyFragment,
                    Bundle().apply {
                        putString("dataId",  action)
                        putString("arrayName", "GraphQL")
                        putString("model", "Reader")
                    })
            }
            "query-Book"-> {
                val model: AssemblyViewModel = viewModel as AssemblyViewModel
                model.queryBook()
            }
            "query-Author"-> {
                val model: AssemblyViewModel = viewModel as AssemblyViewModel
                model.queryAuthor()
            }
            "query-Reader"-> {
                val model: AssemblyViewModel = viewModel as AssemblyViewModel
                model.queryReader()
            }
//       673711
//            "querying-relations-Book"-> {
//                val model: AssemblyViewModel = viewModel as AssemblyViewModel
//                model.queryBookRelations()
//            }
//            "querying-relations-Author"-> {
//                val model: AssemblyViewModel = viewModel as AssemblyViewModel
//                model.queryAuthorRelations()
//            }
            else -> {
                ToastUtils.showShort("该功能开发中")
            }
        }

    }


    fun getAuthUserAttributeKey(): Array<String> {
        val field = AuthUserAttributeKey::class.java.declaredFields
        val date: Array<String> = Array(field.size) { i -> field[i].name }
        return date
    }
}