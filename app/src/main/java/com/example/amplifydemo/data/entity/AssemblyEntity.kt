package com.example.amplifydemo.data.entity

import com.chad.library.adapter.base.entity.MultiItemEntity

class AssemblyEntity(override val itemType: Int, var content: String?,var hint: String?) : MultiItemEntity {
    companion object {
        val TEXT_TITLE = 0
        val TEXT_BUTTON = 1
        val TEXT_INFO = 2
        val EditText = 3
    }




    var title: String? = null

}