package com.example.amplifydemo.ui.adapter.model

import android.graphics.ColorSpace
import com.amplifyframework.core.model.Model
import com.amplifyframework.datastore.generated.model.Author
import com.amplifyframework.datastore.generated.model.Book
import com.amplifyframework.datastore.generated.model.Reader
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.module.BaseDraggableModule
import com.chad.library.adapter.base.module.DraggableModule
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.example.amplifydemo.R
import com.example.amplifydemo.ui.fragment.storage.StorageFragment

class ModelAdapter(data: ArrayList<ColorSpace.Model>) :
    BaseQuickAdapter<ColorSpace.Model, BaseViewHolder>(R.layout.item_categories, data),
    DraggableModule {
    override fun convert(holder: BaseViewHolder, item: ColorSpace.Model) {

        if (item is Book) {
            val book: Book = item
            holder.setText(R.id.tv_name,"id:"+book.id+"\nname:"+book.name+"\nauthor:"+book.author)
        }else if (item is Author){
            val author: Author = item
            holder.setText(R.id.tv_name,"id:"+author.id+"\nname:"+author.name+"\nage:"+author.age)
        }else if (item is Reader){
            val reader: Reader = item
            holder.setText(R.id.tv_name,"id:"+reader.id+"\nname:"+reader.name+"\nage:"+reader.age)
        }

    }


    override fun addDraggableModule(baseQuickAdapter: BaseQuickAdapter<*, *>): BaseDraggableModule {
        return StorageFragment.MyDraggableModule(baseQuickAdapter)
    }



    fun getModel(int: Int):Model{
        if (getItem(int) is Book) {
            val book: Book = getItem(int) as Book

            return  book
        }else if (getItem(int) is Author){
            val author: Author = getItem(int)as Author
            return  author
        }else{
            return getItem(int)
        }
    }
}