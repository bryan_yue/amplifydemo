package com.example.amplifydemo.ui.fragment.assembly

import android.content.Intent
import android.util.Log
import com.amplifyframework.api.graphql.GraphQLRequest
import com.amplifyframework.api.graphql.model.ModelMutation
import com.amplifyframework.api.graphql.model.ModelQuery
import com.amplifyframework.auth.AuthUserAttribute
import com.amplifyframework.auth.AuthUserAttributeKey
import com.amplifyframework.auth.cognito.AWSCognitoAuthSession
import com.amplifyframework.auth.result.AuthSessionResult
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.model.Model
import com.amplifyframework.core.model.query.Where
import com.amplifyframework.datastore.generated.model.Author
import com.amplifyframework.datastore.generated.model.Book
import com.amplifyframework.datastore.generated.model.Reader
import com.amplifyframework.datastore.generated.model.Todo
import com.amplifyframework.storage.StorageAccessLevel
import com.amplifyframework.storage.options.StorageUploadFileOptions
import com.amplifyframework.storage.options.StorageUploadInputStreamOptions
import com.blankj.utilcode.util.FileUtils
import com.example.amplifydemo.app.databind.StringObservableField
import com.example.amplifydemo.app.viewmodel.BaseViewModel
import com.example.amplifydemo.ui.adapter.assembly.AssemblyAdapter
import java.io.File
import java.io.FileInputStream


class AssemblyViewModel : BaseViewModel() {

    var message = StringObservableField()
    var value = StringObservableField()
    val stringBuilder = StringBuilder()

    var assemblyAdapter: AssemblyAdapter? = null

    fun fetchUserAttributes() {
        showDialog()
        Amplify.Auth.fetchUserAttributes(
            {
                stringBuilder.clear()
                stringBuilder.append("获取用户信息成功:")
                it.forEach {
                    stringBuilder.append("\n")
                    stringBuilder.append("key:" + it.key.keyString)
                    stringBuilder.append("\n")
                    stringBuilder.append("value:" + it.value)
                    stringBuilder.append("\n")
                }
                message.set(stringBuilder.toString())
                dismissDialog()
                Log.e("AmplifyDemo", "User attributes = $stringBuilder")
            },
            {
                stringBuilder.clear()
                stringBuilder.append("获取用户信息失败: $it")
                message.set(stringBuilder.toString())
                dismissDialog()
                Log.e("AmplifyDemo", "Failed to fetch user attributes", it)
            }
        )


    }


    fun updateUserAttributes(key: String) {
        val list: ArrayList<AuthUserAttribute> = arrayListOf()
        list.add(AuthUserAttribute(getAuthUserAttributeKey(key), value.get()))
        showDialog()
        Amplify.Auth.updateUserAttributes(
            list, // attributes is a list of AuthUserAttribute
            {

                stringBuilder.clear()
                stringBuilder.append("更新用户信息成功:$it")
                message.set(stringBuilder.toString())
                dismissDialog()
                Log.i("AmplifyDemo", "Updated user attributes = $it")
            },
            {
                stringBuilder.clear()
                stringBuilder.append("更新用户信息成功: $it")
                message.set(stringBuilder.toString())
                dismissDialog()
                Log.e("AmplifyDemo", "Failed to update user attributes", it)
            }
        )


    }


    fun getAuthUserAttributeKey(key: String): AuthUserAttributeKey {
        when (key) {
            "updateUserAttributes ADDRESS" -> {
                return AuthUserAttributeKey.address()
            }
            "updateUserAttributes BIRTHDATE" -> {
                return AuthUserAttributeKey.birthdate()
            }
            else -> {
                return AuthUserAttributeKey.address()
            }
        }
    }

    fun openFileManage(fragment: AssemblyFragment) {
        val intent: Intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.setType("*/*")
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        fragment.startActivityForResult(intent, fragment.RC_EXTERNAL_STORAGE)
    }

    fun uploadInputStream(file: File) {
        showDialog()


        Amplify.Auth.fetchAuthSession(
            {
                val session = it as AWSCognitoAuthSession
                when (session.identityId.type) {
                    AuthSessionResult.Type.SUCCESS -> {
                        Log.i("AmplifyDemo", "IdentityId = ${session.identityId.value}")

                        val options = StorageUploadInputStreamOptions.builder()
                            .accessLevel(StorageAccessLevel.PUBLIC)
                            .build()

                        Log.e("AmplifyDemo", "FileUtils = ${FileUtils.getFileName(file)}")
                        Log.e("AmplifyDemo", "FileUtils = ${FileUtils.getFileExtension(file)}")
                        Log.e("AmplifyDemo", "FileUtils = ${FileUtils.getSize(file)}")

                        val stream = FileInputStream(file)

                        Amplify.Storage.uploadInputStream(FileUtils.getFileName(file),
                            stream,
                            options,
                            {
                                stringBuilder.clear()
                                stringBuilder.append("上传文件流进度:${it.fractionCompleted}")
                                message.set(stringBuilder.toString())
                                Log.i("AmplifyDemo", "Fraction completed: ${it.fractionCompleted}")
                            },
                            {
                                stringBuilder.clear()
                                stringBuilder.append("上传文件流成功:$it")
                                message.set(stringBuilder.toString())
                                dismissDialog()
                                Log.i("AmplifyDemo", "Successfully uploaded: ${it.key}")
                            },
                            {
                                stringBuilder.clear()
                                stringBuilder.append("上传文件流失败:$it")
                                message.set(stringBuilder.toString())
                                dismissDialog()
                                Log.e("AmplifyDemo", "Upload failed", it)
                            }
                        )

                    }

                    AuthSessionResult.Type.FAILURE -> {
                        stringBuilder.clear()
                        stringBuilder.append("上传文件流失败 获取凭证失败:$it")
                        message.set(stringBuilder.toString())
                        dismissDialog()
                        Log.w("AmplifyDemo", "IdentityId not found", session.identityId.error)
                    }

                }
            },
            {
                stringBuilder.clear()
                stringBuilder.append("上传文件流失败 获取凭证失败:$it")
                message.set(stringBuilder.toString())
                dismissDialog()

                Log.e("AmplifyDemo", "Failed to fetch session", it)


            }
        )

    }

    fun uploadFile(file: File) {
        showDialog()


        Amplify.Auth.fetchAuthSession(
            {
                val session = it as AWSCognitoAuthSession
                when (session.identityId.type) {
                    AuthSessionResult.Type.SUCCESS -> {
                        Log.i("AmplifyDemo", "IdentityId = ${session.identityId.value}")

                        val options = StorageUploadFileOptions.builder()
                            .accessLevel(StorageAccessLevel.PUBLIC)
                            .build()


                        Log.e("AmplifyDemo", "FileUtils = ${FileUtils.getFileName(file)}")
                        Log.e("AmplifyDemo", "FileUtils = ${FileUtils.getFileExtension(file)}")
                        Log.e("AmplifyDemo", "FileUtils = ${FileUtils.getSize(file)}")

                        Amplify.Storage.uploadFile(FileUtils.getFileName(file), file, options,
                            {
                                stringBuilder.clear()
                                stringBuilder.append("上传文件进度:${it.fractionCompleted}")
                                message.set(stringBuilder.toString())
                                Log.i("AmplifyDemo", "Fraction completed: ${it.fractionCompleted}")
                            },
                            {
                                stringBuilder.clear()
                                stringBuilder.append("上传文件成功:$it")
                                message.set(stringBuilder.toString())
                                dismissDialog()
                                Log.i("AmplifyDemo", "Successfully uploaded: ${it.key}")
                            },
                            {
                                stringBuilder.clear()
                                stringBuilder.append("上传文件失败:$it")
                                message.set(stringBuilder.toString())
                                dismissDialog()
                                Log.e("AmplifyDemo", "Upload failed", it)
                            }
                        )


                    }

                    AuthSessionResult.Type.FAILURE -> {
                        stringBuilder.clear()
                        stringBuilder.append("上传文件失败 获取凭证失败:$it")
                        message.set(stringBuilder.toString())
                        dismissDialog()
                        Log.w("AmplifyDemo", "IdentityId not found", session.identityId.error)
                    }

                }
            },
            {
                stringBuilder.clear()
                stringBuilder.append("上传文件失败 获取凭证失败:$it")
                message.set(stringBuilder.toString())
                dismissDialog()

                Log.e("AmplifyDemo", "Failed to fetch session", it)


            }
        )

    }


    fun fetchAuthSession() {
        showDialog()
        Amplify.Auth.fetchAuthSession(
            {

                stringBuilder.clear()
                stringBuilder.append("获取凭证成功: Auth session \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
                Log.i("AmplifyDemo", "Auth session = $it")
            },
            {
                stringBuilder.clear()
                stringBuilder.append("获取凭证失败: Failed to fetch auth session \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
                Log.e("AmplifyDemo", "Failed to fetch auth session", it)
            }
        )
    }


    fun createandupdateBook(name: String, author: String, description: String, status: String) {
        showDialog()
        var item: Book = Book.builder()
            .name(name)
            .author(author)
            .description(description)
            .status(status)
            .build()
        SaveDataStore(item)
    }


    fun createandupdateAuthor(name: String, age: String, gender: String) {
        showDialog()

        var item: Author = Author.builder()
            .name(name)
            .age(age.toInt())
            .gender(gender)
            .build()

        queryAuthorBY(name, item)
    }


    fun SaveDataStore(item: Model) {
        Amplify.DataStore.save(
            item,
            {
                Log.i("AmplifyDemo", "Saved item Successfully: " + it.item().toString())
                stringBuilder.clear()
                stringBuilder.append("Saved item Successfully: \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()

            },
            {
                Log.e("AmplifyDemo", "Could not save item to DataStore", it)
                stringBuilder.clear()
                stringBuilder.append("Could not save item to DataStore \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )
    }


    fun queryBook() {
        showDialog()
        Amplify.DataStore.query(
            Book::class.java,
            Where.matches(Book.NAME.contains(value.get())),
            { items ->
                stringBuilder.clear()
                stringBuilder.append("queryBook  Successfully: \n")
                var i = 0
                while (items.hasNext()) {
                    i++
                    val book = items.next()
                    stringBuilder.append("id:" + book.id + "\nname:" + book.name + "\nauthor:" + book.author + "\n\n")
                }
                stringBuilder.append("queryBook  size: ${i}\n")
                message.set(stringBuilder.toString())
                dismissDialog()


            },
            {
                Log.e("AmplifyDemo", "Could not query DataStore", it)
                stringBuilder.clear()
                stringBuilder.append("Could not query DataStore \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )
    }

    fun queryBookBY(name: String, book: Book) {
        Amplify.DataStore.query(
            Book::class.java,
            Where.matches(Book.NAME.contains(value.get())),
            { items ->
                stringBuilder.clear()
                stringBuilder.append("queryBook  Successfully: \n")
                var copyAuthor: Book? = null
                while (items.hasNext()) {
                    val cach = items.next()
                    copyAuthor = cach.copyOfBuilder()
                        .name(book.name)
                        .description(book.description)
                        .author(book.author)
                        .status(book.status)
                        .build()
                }
                if (copyAuthor == null) {
                    Log.e("AmplifyDemo", "queryAuthorBY create Book ")
                    SaveDataStore(book)
                } else {
                    Log.e("AmplifyDemo", "queryAuthorBY  update Book")

                    SaveDataStore(copyAuthor)
                }


            },
            {
                Log.e("AmplifyDemo", "Could not query DataStore", it)
                stringBuilder.clear()
                stringBuilder.append("Could not query DataStore \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )
    }

    fun queryAuthor() {
        showDialog()
        Amplify.DataStore.query(
            Author::class.java,
            Where.matches(Author.NAME.contains(value.get())),
            { items ->
                stringBuilder.clear()
                stringBuilder.append("queryAuthor  Successfully: \n")
                var i = 0
                while (items.hasNext()) {
                    i++
                    val book = items.next()
                    stringBuilder.append("id:" + book.id + "\nname:" + book.name + "\nage:" + book.age + "\n\n")
                }
                stringBuilder.append("queryAuthor size: ${i}\n")
                message.set(stringBuilder.toString())
                dismissDialog()


            },
            {
                Log.e("AmplifyDemo", "Could not query DataStore", it)
                stringBuilder.clear()
                stringBuilder.append("Could not query DataStore \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )
    }

    fun queryAuthorBY(name: String, author: Author) {
        Amplify.DataStore.query(
            Author::class.java,
            Where.matches(Author.NAME.eq(name)),
            { items ->
                var copyAuthor: Author? = null
                while (items.hasNext()) {
                    val cach = items.next()
                    copyAuthor = cach.copyOfBuilder()
                        .name(author.name)
                        .age(author.age)
                        .gender(author.gender)
                        .build()
                }
                if (copyAuthor == null) {
                    Log.e("AmplifyDemo", "queryAuthorBY create Author ")
                    SaveDataStore(author)
                } else {
                    Log.e("AmplifyDemo", "queryAuthorBY  update Author")

                    SaveDataStore(copyAuthor)
                }

            },
            {
                Log.e("AmplifyDemo", "Could not query DataStore", it)
                stringBuilder.clear()
                stringBuilder.append("Could not query DataStore \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )
    }

    fun queryBookRelations() {
        Amplify.DataStore.query(Book::class.java, Author.NAME.eq(value.get()),
            { matches ->
                stringBuilder.clear()
                stringBuilder.append("queryAuthor  Successfully: \n")
                var i = 0
                while (matches.hasNext()) {
                    i++
                    val book = matches.next()
                    stringBuilder.append("id:" + book.id + "\nname:" + book.name + "\nauthor:" + book.author + "\n\n")
                }
                stringBuilder.append("queryAuthor size: ${i}\n")
                message.set(stringBuilder.toString())
                dismissDialog()
            },
            { Log.e("MyAmplifyApp", "Query failed", it) }
        )
    }

    fun queryAuthorRelations() {
        Amplify.DataStore.query(Author::class.java, Book.NAME.eq(value.get()),
            { matches ->
                stringBuilder.clear()
                stringBuilder.append("queryAuthor  Successfully: \n")
                var i = 0
                while (matches.hasNext()) {
                    i++
                    val book = matches.next()
                    stringBuilder.append("id:" + book.id + "\nname:" + book.name + "\nage:" + book.age + "\n\n")
                }
                stringBuilder.append("queryAuthor size: ${i}\n")
                message.set(stringBuilder.toString())
                dismissDialog()
            },
            { Log.e("MyAmplifyApp", "Query failed", it) }
        )
    }


    fun createandupdateReader(name: String, age: String, gender: String) {
        showDialog()

        var item: Reader = Reader.builder()
            .name(name)
            .age(age.toInt())
            .gender(gender)
            .build()

        queryReaderBY(name, item)
    }


    fun GraphQL_Creat(item: Model) {

        Amplify.API.mutate(ModelMutation.create(item),
            {
                Log.i("AmplifyDemo",
                    "GraphQL creat ${it.data.javaClass.simpleName} Successfully: \n" + it.data.toString())
                stringBuilder.clear()
                stringBuilder.append("GraphQL creat ${it.data.javaClass.simpleName} Successfully: \n")
                stringBuilder.append(it.data.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            },
            {
                Log.e("AmplifyDemo", "Could not creat ${item.javaClass.simpleName} to GraphQL", it)
                stringBuilder.clear()
                stringBuilder.append("Could not creat ${item.javaClass.simpleName} to GraphQL\n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )

    }

    fun GraphQL_Update(item: Model) {
        Amplify.API.mutate(ModelMutation.update(item),
            {
                Log.i("AmplifyDemo",
                    "GraphQL update ${it.data.javaClass.simpleName} Successfully: \n" + it.data.toString())
                stringBuilder.clear()
                stringBuilder.append("GraphQL creat ${it.data.javaClass.simpleName} Successfully: \n")
                stringBuilder.append(it.data.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            },
            {
                Log.e("AmplifyDemo", "Could not update ${item.javaClass.simpleName} to GraphQL", it)
                stringBuilder.clear()
                stringBuilder.append("Could not update ${item.javaClass.simpleName} to GraphQL\n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )

    }

    fun GraphQL_Delete(item: Model) {
        Amplify.API.mutate(ModelMutation.delete(item),
            {
                Log.i("AmplifyDemo",
                    "GraphQL delete ${it.data.javaClass.simpleName} Successfully: \n" + it.data.toString())
                stringBuilder.clear()
                stringBuilder.append("GraphQL creat ${it.data.javaClass.simpleName} Successfully: \n")
                stringBuilder.append(it.data.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            },
            {
                Log.e("AmplifyDemo", "Could not delete ${item.javaClass.simpleName} to GraphQL", it)
                stringBuilder.clear()
                stringBuilder.append("Could not delete ${item.javaClass.simpleName} to GraphQL \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )

    }


    fun queryReader() {
        showDialog()
        Amplify.API.query(
            ModelQuery.list(Reader::class.java, Reader.NAME.contains(value.get())),
            { response ->
                stringBuilder.clear()
                stringBuilder.append("queryReader  Successfully: \n")
                var i = 0
                response.data.forEach { reader ->
                    i++
                    stringBuilder.append("id:" + reader.id + "\nname:" + reader.name + "\nage:" + reader.age + "\n\n")
                }
                stringBuilder.append("queryReader size: ${i}\n")
                message.set(stringBuilder.toString())
                dismissDialog()
            },
            {
                Log.e("MyAmplifyApp", "Query failure", it)
                stringBuilder.clear()
                stringBuilder.append("Could not query GraphQL \n")
                stringBuilder.append(it.toString())
                message.set(stringBuilder.toString())
                dismissDialog()
            }
        )

    }


    fun queryReaderBY(name: String, reader: Reader) {
        Amplify.API.query(
            ModelQuery.list(Reader::class.java, Reader.NAME.eq(name)),
            { response ->
                var copyReader: Reader? = null

                for (bean: Reader in response.data) {
                    Log.i("AmplifyDemo", bean.name)
                    copyReader =
                        bean.copyOfBuilder().name(reader.name).age(reader.age).gender(reader.gender)
                            .build()

                }

                if (copyReader == null) {
                    Log.e("AmplifyDemo", "queryReaderBY create Reader ")
                    Log.i("AmplifyDemo", copyReader.toString())
                    GraphQL_Creat(reader)
                } else {
                    Log.e("AmplifyDemo", "queryReaderBY  update Reader")
                    Log.i("AmplifyDemo", copyReader.toString())

                    GraphQL_Update(copyReader)
                }
            },
            { Log.e("MyAmplifyApp", "Query failure", it) }
        )
    }
}