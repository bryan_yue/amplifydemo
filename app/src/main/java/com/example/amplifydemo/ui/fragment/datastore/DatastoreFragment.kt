package com.example.amplifydemo.ui.fragment.datastore

import android.graphics.Canvas
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.amplifyframework.api.graphql.model.ModelMutation
import com.amplifyframework.api.graphql.model.ModelQuery
import com.amplifyframework.api.graphql.model.ModelSubscription
import com.amplifyframework.core.Action
import com.amplifyframework.core.Amplify
import com.amplifyframework.core.Consumer
import com.amplifyframework.core.async.Cancelable
import com.amplifyframework.core.model.query.*
import com.amplifyframework.core.model.query.predicate.QueryPredicate
import com.amplifyframework.datastore.DataStoreException
import com.amplifyframework.datastore.DataStoreQuerySnapshot
import com.amplifyframework.datastore.generated.model.Author
import com.amplifyframework.datastore.generated.model.Book
import com.amplifyframework.datastore.generated.model.Reader
import com.amplifyframework.datastore.generated.model.Todo
import com.blankj.utilcode.util.ToastUtils
import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.listener.OnItemSwipeListener
import com.chad.library.adapter.base.module.BaseDraggableModule
import com.example.amplifydemo.R
import com.example.amplifydemo.app.ext.init
import com.example.amplifydemo.base.BaseFragment
import com.example.amplifydemo.databinding.FragmentRecyclerviewBinding
import com.example.amplifydemo.ui.fragment.categories.CategoriesViewModel
import kotlinx.android.synthetic.main.fragment_recyclerview.*
import kotlin.collections.ArrayList

import com.example.amplifydemo.ui.adapter.model.ModelAdapter


class DatastoreFragment : BaseFragment<CategoriesViewModel, FragmentRecyclerviewBinding>() {

    private val modelAdapter: ModelAdapter by lazy { ModelAdapter(arrayListOf()) }
    var error: View? = null


    override fun layoutId(): Int {
        return R.layout.fragment_recyclerview
    }

    override fun initView(savedInstanceState: Bundle?) {

        recyclerView.init(LinearLayoutManager(context), modelAdapter).let {
            it.addItemDecoration(
                DividerItemDecoration(
                    context,
                    LinearLayoutManager.VERTICAL
                )
            )
        }
        error = layoutInflater.inflate(R.layout.item_text_info, recyclerView, false)
        error?.let {
            val info: TextView = it.findViewById(R.id.tv_info)
            info.text = "点击进入编辑页面，侧滑删除数据"
            modelAdapter.addHeaderView(it)
        }


        modelAdapter.headerWithEmptyEnable = true
        modelAdapter.run {

            draggableModule.isSwipeEnabled = true
            draggableModule.itemTouchHelperCallback.setSwipeMoveFlags(ItemTouchHelper.START or ItemTouchHelper.END)
            draggableModule.setOnItemSwipeListener(object : OnItemSwipeListener {
                override fun onItemSwipeStart(viewHolder: RecyclerView.ViewHolder?, pos: Int) {
                    Log.e("onItemSwipe ", "Start")

                }

                override fun clearView(viewHolder: RecyclerView.ViewHolder?, pos: Int) {
                }

                override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder?, pos: Int) {
                    Log.e("onItemSwipe ", "End")
                    showLoading("数据正在删除。。。")
                    if (modelAdapter.getModel(pos) is Reader) {
                        Amplify.API.mutate(ModelMutation.delete(modelAdapter.getModel(pos)),
                            {
                                Log.i("AmplifyDemo",
                                    "GraphQL delete ${modelAdapter.getModel(pos).javaClass.simpleName} Successfully: \n" + it.toString())
                                recyclerView.post {
                                    dismissLoading()
                                    removeAt(pos)
                                    ToastUtils.showShort("Successfully removed: ${it}")
                                }
                            },
                            {
                                Log.e("AmplifyDemo",
                                    "Could not delete ${modelAdapter.getModel(pos).javaClass.simpleName} to GraphQL",
                                    it)
                                recyclerView.post {
                                    dismissLoading()
                                    notifyItemChanged(pos)
                                    ToastUtils.showShort("Remove failure", it)
                                }
                            })
                    } else {
                        Amplify.DataStore.delete(modelAdapter.getModel(pos),
                            {
                                Log.i("Amplify", "Deleted item.")
                                recyclerView.post {
                                    dismissLoading()
                                    removeAt(pos)
                                    ToastUtils.showShort("Successfully removed: ${it}")
                                }
                            },
                            {
                                Log.e("Amplify", "Delete failed.", it)
                                recyclerView.post {
                                    dismissLoading()
                                    notifyItemChanged(pos)
                                    ToastUtils.showShort("Remove failure", it)
                                }
                            }
                        )
                    }


//                    val options = StorageRemoveOptions.builder()
//                        .accessLevel(StorageAccessLevel.PUBLIC)
//                        .build()
//
//                    Amplify.Storage.remove(getItem(pos).key, options,
//                        {
//                            recyclerView.post {
//                                dismissLoading()
//                                removeAt(pos)
//                                ToastUtils.showShort("Successfully removed: ${it.key}")
//                            }
//                            Log.i("AmplifyDemo", "Successfully removed: ${it.key}")
//                        },
//                        {
//
//                            recyclerView.post {
//                                dismissLoading()
//                                notifyItemChanged(pos)
//                                ToastUtils.showShort("Remove failure", it)
//                            }
//                            Log.e("AmplifyDemo", "Remove failure", it)
//                        }
//                    )


                }

                override fun onItemSwipeMoving(
                    canvas: Canvas?,
                    viewHolder: RecyclerView.ViewHolder?,
                    dX: Float,
                    dY: Float,
                    isCurrentlyActive: Boolean,
                ) {
                }
            })



            setOnItemClickListener { adapter, view, position ->

            }


        }


    }

    class MyDraggableModule(baseQuickAdapter: BaseQuickAdapter<*, *>) :
        BaseDraggableModule(baseQuickAdapter) {
        override fun onItemSwiped(viewHolder: RecyclerView.ViewHolder) {
//            super.onItemSwiped(viewHolder)
            val pos = getViewHolderPosition(viewHolder)
            if (isSwipeEnabled) {
                mOnItemSwipeListener?.onItemSwiped(viewHolder, pos)
            }
        }
    }

    override fun createObserver() {
    }


    override fun initData() {

        val dataId = arguments?.getString("dataId", null)
        val arrayName = arguments?.getString("arrayName", null)
        val model = arguments?.getString("model", null)
        if (!TextUtils.isEmpty(model)) {
            showLoading("Loading...")
            when (model) {
                "Book" -> {

                    Amplify.DataStore.query(
                        Book::class.java,
                        {
                            val list: ArrayList<Book> = arrayListOf()
                            while (it.hasNext()) {
                                val book = it.next()
                                list.add(book)
                                Log.i("Amplify", "Queried item: " + book.id)
                            }
                            recyclerView.post {
                                modelAdapter.setNewInstance(list.toCollection(ArrayList()))
                                dismissLoading()
                            }

                        },
                        {
                            recyclerView.post {
                                dismissLoading()
                                val errorS: String = it.toString()
                                error?.let { it ->
                                    val info: TextView = it.findViewById(R.id.tv_info)
                                    info.text = "Could not query DataStore\n" + errorS
                                }

                            }
                            Log.e("AmplifyDemo", "Could not query DataStore", it)
                        }
                    )
                }
                "Author" -> {
                    Amplify.DataStore.query(
                        Author::class.java,
                        {
                            val list: ArrayList<Author> = arrayListOf()
                            while (it.hasNext()) {
                                val author = it.next()
                                list.add(author)
                                Log.i("AmplifyDemo", "Queried item: " + author.id)
                            }


                            recyclerView.post {
                                modelAdapter.setNewInstance(list.toCollection(ArrayList()))
                                dismissLoading()
                            }

                        },
                        {

                            recyclerView.post {
                                dismissLoading()
                                val errorS: String = it.toString()
                                error?.let { it ->
                                    val info: TextView = it.findViewById(R.id.tv_info)
                                    info.text = "Could not query DataStore\n" + errorS
                                }

                            }
                            Log.e("AmplifyDemo", "Could not query DataStore", it)
                        }
                    )
                }
                "Reader" -> {
                    Amplify.API.query(
                        ModelQuery.list(Reader::class.java),
                        { response ->
                            val list: ArrayList<Reader> = arrayListOf()
                            response.data.forEach { reader ->
                                list.add(reader)
                                Log.i("AmplifyDemo", "API (GraphQL) Queried  item: " + reader.id)
                            }
                            recyclerView.post {
                                modelAdapter.setNewInstance(list.toCollection(ArrayList()))
                                dismissLoading()
                            }
                        },
                        {
                            recyclerView.post {
                                dismissLoading()
                                val errorS: String = it.toString()
                                error?.let { it ->
                                    val info: TextView = it.findViewById(R.id.tv_info)
                                    info.text = "Could not query API (GraphQL)\n" + errorS
                                }

                            }

                            Log.e("AmplifyDemo", "Could not query API (GraphQL)", it)
                        }
                    )
                }
                else -> {
                    dismissLoading()
                }
            }



            arrayName?.let {
                when (it) {
                    "DataStore" -> {
                        val tag = "ObserveQuery"
                        val onQuerySnapshot: Consumer<DataStoreQuerySnapshot<Author>> =
                            Consumer<DataStoreQuerySnapshot<Author>> { value: DataStoreQuerySnapshot<Author> ->
                                Log.d(tag, "success on snapshot")
                                Log.d(tag, "number of records: " + value.items.size)
                                Log.d(tag, "sync status: " + value.isSynced)
                            }

                        val observationStarted =
                            Consumer { _: Cancelable ->
                                Log.d(tag, "success on cancelable")
                            }
                        val onObservationError =
                            Consumer { value: DataStoreException ->
                                Log.d(tag, "error on snapshot $value")
                            }
                        val onObservationComplete = Action {
                            Log.d(tag, "complete")
                        }

                        val predicate: QueryPredicate =
                            Author.NAME.beginsWith("Y").and(Author.AGE.lt(30))
                        val querySortBy = QuerySortBy("post", "rating", QuerySortOrder.ASCENDING)

                        val options = ObserveQueryOptions(predicate, listOf(querySortBy))
                        Amplify.DataStore.observeQuery(
                            Author::class.java,
                            options,
                            observationStarted,
                            onQuerySnapshot,
                            onObservationError,
                            onObservationComplete
                        )

                    }
                    "GraphQL" -> {
                        val subscription = Amplify.API.subscribe(
                            ModelSubscription.onCreate(Reader::class.java),
                            { Log.i("ApiQuickStart", "Subscription established") },
                            { Log.i("ApiQuickStart", "Todo create subscription received: ${(it.data as Reader).name}") },
                            { Log.e("ApiQuickStart", "Subscription failed", it) },
                            { Log.i("ApiQuickStart", "Subscription completed") }
                        )

                        subscription?.cancel()
                    }
                    else -> {

                    }
                }
            }
        }


    }
}